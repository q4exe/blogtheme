<?php wp_footer(); ?>
    <div class="footer">
        <div class="col-4 d-flex justify-content-end align-items-center">
            <?php wp_nav_menu(['menu' => 'Site navigation',
                'menu_class' => ' nav d-flex justify-content-end pt-3',
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',]); ?>
        </div>
    </div>
</div>
</body>
</html>
