<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>A blog about wildlife that surrounds us</title>
    <<meta name="keywords" content="global warming water earth nature solar energy polar bears recycling pollution green solar power endangered species air pollution water pollution solar panels electric cars wind energy climate change tankless water heater wind powerplanet">
    <meta http-equiv="content-language" content="en">
    <meta name="robots" content="index, follow">
    <meta name="description" content="We always polluted our surroundings. But until now pollution was not such a huge problem. People lived in the countryside and couldn’t produce such amount of pollution that would lead to a dangerous situation on a global scale.">
    <meta name="author" content="Kryvkin Anatolii">
    <meta name="copyright" content="All rights belong to Anatoly Kryvkin">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <script src="https://kit.fontawesome.com/e9b10c8170.js" crossorigin="anonymous"></script>



    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo get_stylesheet_uri() ?>" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body>

<div class="container">
    <header class="blog-header py-3">
        <div class="row flex-nowrap justify-content-between align-items-center">
            <div class="col-1pt-1">
                <a class="text-white" href="<?php echo get_home_url( null, 'home/', 'https' ); ?>" style="font-size: 20px" ><i class="far fa-newspaper"></i></a>
            </div>
            <div class="col-5 col-md-4 col-xl-3 text-center">
                <a class="blog-header-logo" href="<?php echo get_home_url( null, 'home/', 'https' ); ?>">theDeveducationWorld</a>
            </div>
            <div class="col-6 col-lg-5 col-xl-4 d-flex justify-content-end align-items-center">
                <?php wp_nav_menu(['menu' => 'Site navigation',
                'menu_class' => ' nav d-flex justify-content-end pt-3',
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'theme_location' => 'primary']); ?>
            </div>
        </div>
    </header>