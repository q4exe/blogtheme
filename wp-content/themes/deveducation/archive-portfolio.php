<?php get_header(); ?>
    <h1 class="title-page">Portfolio</h1>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="blog-post rounded mt-2 p-2 bg-white">
        <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
            <h3 class="storytitle"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
            <?php the_author() ?>@ <?php the_time() ?>
            <div class="post-thumb"><?php the_post_thumbnail('post-thumb') ?></div>
            <div class="storycontent">
                <?php the_content(); ?>
            </div>
            <?php
            $cur_terms = get_the_terms($post->ID, 'lesson');
            if (is_array($cur_terms)) {
                foreach ($cur_terms as $cur_term) {
                    echo 'Tags: <a href="' . get_term_link($cur_term->term_id, $cur_term->taxonomy) . '">' . $cur_term->name . '</a> ';
                }
            }
            ?>
        </div>
    </div>
<?php endwhile; else: ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

<?php posts_nav_link(' — ', __('« Newer Posts'), __('Older Posts »')); ?>
<?php
get_footer();
