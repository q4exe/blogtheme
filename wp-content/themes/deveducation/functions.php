<?php
add_action('after_setup_theme', 'theme_register_nav_menu');
function theme_register_nav_menu()
{
    register_nav_menu('primary', 'Primary Menu');
    register_nav_menu('footer', 'Footer Menu');
    add_theme_support( 'post-thumbnails', array( 'post' ) );
    add_image_size('post_thumb', 1300, 500, false);
            add_filter( 'excerpt_more', 'new_excerpt_more' );
            function new_excerpt_more( $more ){
                global $post;
                return '<a href="'. get_permalink($post) . '"> Read more...</a>';
            }

    add_filter('navigation_markup_template', 'my_navigation_template', 10, 2 );
    function my_navigation_template( $template, $class ){
        /*
        Вид базового шаблона:
        <nav class="navigation %1$s" role="navigation">
            <h2 class="screen-reader-text">%2$s</h2>
            <div class="nav-links">%3$s</div>
        </nav>
        */

        return '
	<nav class="navigation %1$s" role="navigation">
		<div class="nav-links">%3$s</div>
	</nav>    
	';
    }
}

function wpschool_create_posttype() {
    register_post_type('lecture',
        array(
            'labels' => array(
                'name' => __('Lectures'),
                'singular_name' => __('Lectures'),
                'menu_name' => __('Lectures', 'root'),
                'all_items' => __('All lectures', 'root'),
                'view_item' => __('Read lecture', 'root'),
                'add_new_item' => __('Add new lecture', 'root'),
                'add_new' => __('Add lecture', 'root'),
                'edit_item' => __('Edit lecture', 'root'),
                'update_item' => __('Update lecture', 'root'),
                'search_items' => __('Find lecture', 'root'),
                'not_found' => __('Lecture is not found', 'root'),
                'not_found_in_trash' => __('Lecture is not found in the bin', 'root'),
            ),
            'public' => true,
            'has_archive' => true,
            'description' => __('Lecture catalog', 'root'),
            'rewrite' => array('slug' => 'lecture'),
            'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
            'taxonomies' => array('lecture'),
        )
    );
    register_post_type('portfolio', array(
        'labels' => array(
            'name' => 'Portfolios',
            'singular_name' => 'Portfolio',
            'add_new' => 'Add new',
            'add_new_item' => 'Add new work',
            'edit_item' => 'Edit work',
            'new_item' => 'New work',
            'view_item' => 'View work',
            'search_items' => 'Find work',
            'not_found' => 'Books is not found',
            'not_found_in_trash' => 'There aren\'t books in the trash bin!',
            'parent_item_colon' => '',
            'menu_name' => 'Portfolio'

        ),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'portfolio'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
    ));
}
add_action( 'init', 'wpschool_create_posttype' );

add_action( 'init', 'create_taxonomy' );
function create_taxonomy(){

    // список параметров: wp-kama.ru/function/get_taxonomy_labels
    register_taxonomy( 'lesson', [ 'lecture', 'portfolio' ], [
        'label'                 => '', // определяется параметром $labels->name
        'labels'                => [
            'name'              => 'Tags',
            'singular_name'     => 'Tag',
            'search_items'      => 'Search Genres',
            'all_items'         => 'All Genres',
            'view_item '        => 'View Genre',
            'parent_item'       => 'Parent Genre',
            'parent_item_colon' => 'Parent Genre:',
            'edit_item'         => 'Edit Genre',
            'update_item'       => 'Update Genre',
            'add_new_item'      => 'Add New Genre',
            'new_item_name'     => 'New Genre Name',
            'menu_name'         => 'Tags',
        ],
        'description'           => '', // описание таксономии
        'public'                => true,
        'hierarchical'          => false,
        'rewrite'               => true,
        'capabilities'          => array(),
        'meta_box_cb'           => null,
        'show_admin_column'     => false,
        'show_in_rest'          => null, // добавить в REST API
        'rest_base'             => null, // $taxonomy
        // '_builtin'              => false,
        //'update_count_callback' => '_update_post_term_count',
    ] );
}

add_action( 'widgets_init', 'register_my_widgets' );
function register_my_widgets(){
    register_sidebar( array(
        'name'          => sprintf(__('Sidebar %d'), 1 ),
        'id'            => "sidebar-1",
        'description'   => '',
        'class'         => '',
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => "</li>\n",
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => "</h2>\n",
    ) );
}

//function add_lesson_to_query( $query ) {
//    if ( is_home() && $query->is_main_query() )
//        $query->set( 'post_type', array( 'post', lesson) );
//    return $query;
//}
//add_action( 'pre_get_posts', 'add_lesson_to_query' );

