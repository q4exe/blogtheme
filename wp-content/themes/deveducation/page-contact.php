<?php get_header(); ?>
<h1 class="text-center text-white pt-4" style="text-shadow: 1px 1px 3px black;">CONTACT</h1><br>
<h2 class="text-center text-white p-0" style="text-shadow: 1px 1px 3px black;">US</h2>
<div class = "contact-form">
    <?php echo do_shortcode(' [contact-form-7 id="5" title="Контактная форма 1"]');?>
</div>
<?php get_footer(); ?>