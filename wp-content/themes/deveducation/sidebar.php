<aside class="mt-4 col-md-2 blog-sidebar">
    <div class="p-1 mb-3 bg-light rounded">
        <?php register_sidebar(); ?>
        <?php dynamic_sidebar(); ?>
    </div>
</aside>
