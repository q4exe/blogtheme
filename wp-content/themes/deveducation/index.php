<?php get_header(); ?>
    <div class="banner-flex d-flex row justify-content-between align-items-top">
        <div class="jumbotron col-md-9 p-4 p-md-5 ml-3 text-white rounded banner">
            <div class="col-md-6 px-0">
                <h1 class="display-4 font-italic" style="text-shadow: 1px 1px 5px black">Take a breath in a wild world.</h1>
                <p class="lead my-3" style="text-shadow: 1px 1px 4px black">Welcome to the wild world!<br>
                    join us to :<br>
                    explore<br>
                    learn new & dive into the world of worldlife.</p>
                <p class="lead mb-0"></p>
            </div>
        </div>
        <?php get_sidebar(); ?>
    </div>
<?php //masterslider(1); ?>
<br>
<?php get_footer(); ?>
    