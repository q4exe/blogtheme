<?php get_header(); ?>
    <main role="main" class="container">
        <div class="row">
            <h1 class="title-page">Articles about nature</h1>
            <?php
            if (have_posts()) : while (have_posts()) :the_post();
                get_template_part('template/template-blog', get_post_format());
            endwhile;
                the_posts_pagination();
            endif;
            ?>
        </div>


    </main>

<?php get_footer(); ?>