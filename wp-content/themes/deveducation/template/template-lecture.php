<div class=" blog-main">
    <div class="blog-post rounded mt-2 p-2 bg-white">
        <h2 class="blog-post-title"><?php the_title(); ?>   </h2>
        <?php the_content(); ?>
        <div class="d-flex row justify-content-between px-3">
            <p class="blog-post-meta"><?php the_date(); ?> <?php the_tags(); ?><a href="#"><?php the_author(); ?></a>
            </p>
        </div>
        <?php
        $cur_terms = get_the_terms($post->ID, 'lesson');
        if (is_array($cur_terms)) {
            foreach ($cur_terms as $cur_term) {
                echo 'Tags: <a href="' . get_term_link($cur_term->term_id, $cur_term->taxonomy) . '">' . $cur_term->name . '</a> ';
            }
        }
        ?>
    </div
</div><!-- /.blog-main -->