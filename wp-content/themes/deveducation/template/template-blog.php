<div class="col-md-8 blog-main">
    <div class="blog-post rounded mt-2 p-2 bg-white">
        <div class="post-thumb"><?php the_post_thumbnail('post-thumb') ?></div>
        <h2 class="blog-post-title"><a href="<?php the_permalink($post); ?>"><?php the_title(); ?></a></h2>
        <?php the_excerpt(); ?>
        <div class="d-flex row justify-content-between px-3">
            <p class="blog-post-meta"><?php the_date(); ?> <a href="#"><?php the_author(); ?></a></p>
        </div>
    </div
</div><!-- /.blog-main -->
