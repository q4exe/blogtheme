<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpresstest' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '[Qm^b@vOD#e;at%Y&YD:oVW242!;@2_I{wySgoNwypObag68}gI5a)$J6S*@kKh(' );
define( 'SECURE_AUTH_KEY',  '&BY6uEXXxP{>(~FgXbEY8-cAnDjq5 I;W?![p!??)Wga/7}1aTw[tI 8yUhsG;F ' );
define( 'LOGGED_IN_KEY',    ' h22VU7X0Y{7oYy]JpN@$_<[~a*f?2]y?GzNFa1T-lM_?uXD.l3-SMitmj)_$SSB' );
define( 'NONCE_KEY',        'f|{8ssRyZRUv5/,nGtl@5&6]lIExc)FP=M0c|1<amLzXHo,8/5Vu[s/`5%<T>1b4' );
define( 'AUTH_SALT',        '2U/DAl9y)D_ oVL&hd,bAu2abIMXM.rH6Kr)ny,zz,k<j;6`eTAB7%}kJfs1BY,G' );
define( 'SECURE_AUTH_SALT', '3UYL)XI}}eN~~?,?<.wctHL7q&#~2s# cQXqQOZg?OiZ=n%X|Py;VPX;LMTW`_4t' );
define( 'LOGGED_IN_SALT',   ' }bk|gpXJlUw?K_MM-`d-&{L)HO&0CRx}|9+mJ0a$O9q,r |M+s.xc>d*0SJ`MuR' );
define( 'NONCE_SALT',       'O4z](0q2BCH?~`zIixfX^bt`?2Na, ;:Zt)J+lO?vgZ:~U)>k3jtq6SZrBi18/sN' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
